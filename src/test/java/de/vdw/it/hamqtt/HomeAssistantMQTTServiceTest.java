package de.vdw.it.hamqtt;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@Slf4j
class HomeAssistantMQTTServiceTest {
  private String password;
  private String host;
  private String user;

  @BeforeEach
  void init() {
    try {
      List<String> readAllLines = Files.readAllLines(new File("mqtt-pw.txt").toPath());
      this.password = readAllLines.get(0);
      this.host = readAllLines.get(1);
      this.user = readAllLines.get(2);
    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }

  @Test
  void testConnect() {
    if (StringUtils.isAnyBlank(this.password, this.host, this.user)) {
      return;
    }

    Settings.builder()
        .sensorName("java-sensor-bridge")
        .mqttHost(this.host)
        .mqttUsername(this.user)
        .mqttPassword(this.password.toCharArray())
        .discoveryTopic("unit_test")
        .build();
  }
}
