package de.vdw.it.hamqtt;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@Slf4j
class AppTest {
  private String password;
  private String host;
  private String user;

  @BeforeEach
  void init() {
    try {
      List<String> readAllLines = Files.readAllLines(new File("mqtt-pw.txt").toPath());
      this.password = readAllLines.get(0);
      this.host = readAllLines.get(1);
      this.user = readAllLines.get(2);
    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }

  @Test
  void testPublish() {
    if (StringUtils.isAnyBlank(this.password, this.host, this.user)) {
      return;
    }
    App app = new App();

    CommandLine cmd = new CommandLine(app);

    // black box testing
    cmd.execute(
        "-d",
        "unit_test",
        "-b",
        "test_topic",
        "-h",
        this.host,
        "-u",
        this.user,
        "-pw",
        this.password,
        "-n",
        "test-temperature",
        "-no",
        "test_node",
        "-id",
        "test_temp",
        "-t",
        "Sensor",
        "unit_of_measurement=C",
        "-no",
        "test_node",
        "-id",
        "test_temp_2",
        "-t",
        "Sensor",
        "unit_of_measurement=C",
        "-v",
        "test_temp_2=31",
        "-v",
        "test_temp=13");
  }
}
