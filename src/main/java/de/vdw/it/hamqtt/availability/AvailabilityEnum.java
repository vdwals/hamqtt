package de.vdw.it.hamqtt.availability;

import de.vdw.it.hamqtt.EnumValue;
import lombok.AllArgsConstructor;

import java.nio.charset.StandardCharsets;

@AllArgsConstructor
public enum AvailabilityEnum implements EnumValue {
  ONLINE,
  OFFLINE;

  public String toString() {
    return name().toLowerCase();
  }

  public byte[] toBytes() {
    return toString().getBytes(StandardCharsets.UTF_8);
  }
}
