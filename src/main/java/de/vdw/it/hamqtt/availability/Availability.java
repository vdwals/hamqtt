package de.vdw.it.hamqtt.availability;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Builder
@Value
public class Availability {

  @NonNull String topic;
  @Builder.Default @NonNull String payloadAvailable = AvailabilityEnum.ONLINE.toString();
  @Builder.Default @NonNull String payloadNotAvailable = AvailabilityEnum.OFFLINE.toString();
}
