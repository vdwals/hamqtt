package de.vdw.it.hamqtt.devices;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections4.map.HashedMap;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.vdw.it.hamqtt.availability.Availability;
import de.vdw.it.hamqtt.devices.entities.AbstractAvailabilityEntity;
import de.vdw.it.hamqtt.devices.entities.AbstractEntity;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Singular;
import lombok.ToString;
import lombok.Value;

@Builder
@Value
public class Device {
  @Singular
  @JsonProperty("cns")
  List<List<String>> connections;

  @Singular
  @JsonProperty("ids")
  List<String> identifiers;

  @JsonProperty("cu")
  String configurationUrl;

  @JsonProperty("mf")
  String manufacturer;

  @JsonProperty("mdl")
  String model;

  @JsonProperty("sa")
  String suggestedArea;

  String name;

  @JsonProperty("sw")
  String swVersion;

  @JsonIgnore
  @EqualsAndHashCode.Exclude
  Map<String, List<AbstractEntity>> entities = new HashedMap<>();

  @JsonIgnore
  @NonNull
  String nodeId;

  @JsonIgnore
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  Device viaDevice;

  @Singular
  @JsonIgnore
  Set<Availability> defaultAvailabilities;

  public void addAvailability(Availability availability) {
    this.defaultAvailabilities.add(availability);

    getAvailabilityEntityStream().forEach(entity -> entity.addAvailability(availability));
  }

  public void addEntity(AbstractEntity entity) {
    addEntity(entity, this.nodeId);
  }

  public void addEntity(AbstractEntity entity, String nodeId) {
    entities.merge(nodeId, List.of(entity), (oldList, newList) -> Stream.of(oldList, newList)
        .flatMap(List::stream).collect(Collectors.toList()));

    // Set device and availability to attached entity
    entity.setDevice(this);
    entity.setNodeId(nodeId);
    if (entity instanceof AbstractAvailabilityEntity availabilityEntity) {
      defaultAvailabilities.forEach(availabilityEntity::addAvailability);
    }
  }

  @JsonIgnore
  public Stream<AbstractAvailabilityEntity> getAvailabilityEntityStream() {
    return entities.values().stream().flatMap(List::stream)
        .filter(config -> config instanceof AbstractAvailabilityEntity)
        .map(config -> (AbstractAvailabilityEntity) config);
  }

  public Optional<AbstractEntity> getConfigByObjectId(String objectId) {
    return entities.values().stream().flatMap(List::stream)
        .filter(entity -> entity.getObjectId().equals(objectId)).findFirst();
  }

  public Optional<AbstractAvailabilityEntity> getEntityByUniqueId(String uniqueId) {
    return getAvailabilityEntityStream().filter(entity -> entity.getUniqueId().equals(uniqueId))
        .findFirst();
  }

  @JsonIgnore
  public Map<String, Object> getEntityValues() {
    return getAvailabilityEntityStream().filter(entity -> entity.getValue() != null)
        .collect(Collectors.toMap(AbstractAvailabilityEntity::getObjectId,
            AbstractAvailabilityEntity::getValue));
  }

  @JsonIgnore
  public Map<String, Object> getEntityValues(String nodeId) {
    return getEntities().get(nodeId).stream()
        .filter(config -> config instanceof AbstractAvailabilityEntity)
        .map(config -> (AbstractAvailabilityEntity) config)
        .filter(entity -> entity.getValue() != null).collect(Collectors
            .toMap(AbstractAvailabilityEntity::getObjectId, AbstractAvailabilityEntity::getValue));
  }

  public String getViaDevice() {
    if (viaDevice == null) {
      return null;
    }
    return viaDevice.getIdentifiers().get(0);
  }
}
