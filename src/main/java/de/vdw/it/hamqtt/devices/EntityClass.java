package de.vdw.it.hamqtt.devices;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EntityClass {
  BINARY_SENSOR("binary_sensor"),
  SENSOR("sensor"),
  SWITCH("switch"),
  SELECT("select"),
  NUMBER("number"),
  BUTTON("button"),
  DEVICE_TRIGGER("device_automation"),
  UPDATE("update");
  
  private String className;

  public String toString() {
    return className;
  }
}
