package de.vdw.it.hamqtt.devices;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("unused")
@AllArgsConstructor
public enum Units {
    PERCENT("%"),
    PARS_PER_MILLION("ppm"),
    AMPERE("A"),
    WATT("W"),
    KILO_WATT("k" + WATT.unit),
    WATT_PER_HOUR(WATT.unit + "h"),
    KILO_WATT_PER_HOUR("k" + WATT_PER_HOUR.unit),
    MEGA_WATT_PER_HOUR("M" + WATT_PER_HOUR.unit),
    HZ("Hz"),
    KILO_HZ("k" + HZ.unit),
    MEGA_HZ("M" + HZ.unit),
    GIGA_HZ("G" + HZ.unit),
    CUBIC_METER("m³"),
    CUBIC_FEET("ft³"),
    LUX("lx"),
    LUMEN("lm"),
    MICRO_GRAM_PER_CUBIC_METER("µg/" + CUBIC_METER.unit),
    CELSIUS("°C"),
    FAHRENHEIT("°F"),
    VOLT("V"),
    DECIBEL("dB"),
    DECIBEL_METER(DECIBEL + "m"),
    BAR("bar"),
    CENTI_BAR("c" + BAR.unit),
    PASCAL("Pa"),
    HECTO_PASCAL("h" + PASCAL.unit),
    KILO_PASCAL("k" + PASCAL.unit),
    MILLI_BAR("m" + BAR.unit),
    INHG("inHg"),
    PSI("psi");
    
    @Getter private String unit;
}
