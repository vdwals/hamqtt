package de.vdw.it.hamqtt.devices.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.vdw.it.hamqtt.utils.TopicUtils;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public abstract class AbstractCommandEntity extends AbstractAvailabilityEntity {
  /** The MQTT topic to publish commands to change the number. */
  String commandTopic;

  /** Flag which defines if the entity should be enabled when first added. */
  Boolean enabledByDefault;

  /**
   * Flag that defines if number works in optimistic mode. Default: true if no state_topic defined,
   * else false.
   */
  @JsonProperty("opt")
  Boolean optimistic;

  /** If the published message should have the retain flag on or not. */
  @JsonProperty("ret")
  Boolean retain;

  /**
   * Defines a template to generate the payload to send to command_topic.
   */
  String commandTemplate;

  @JsonProperty("cmd_t")
  public String getCommandTopic() {
    if (this.commandTopic != null) {
      return this.commandTopic;
    }

    return getRelativeTopic(TopicUtils.buildTopic(getObjectId(), TopicUtils.COMMAND_TOPIC));
  }
}
