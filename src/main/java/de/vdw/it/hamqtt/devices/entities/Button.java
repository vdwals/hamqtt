package de.vdw.it.hamqtt.devices.entities;

import de.vdw.it.hamqtt.devices.EntityClass;
import de.vdw.it.hamqtt.devices.IDeviceClass;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Button extends AbstractCommandEntity {

  public enum DeviceClass implements IDeviceClass {
    restart, update
  }

  DeviceClass deviceClass;

  /** A special payload that resets the state to None when received on the state_topic. */
  Object payload_press;

  @Override
  public EntityClass getClassName() {
    return EntityClass.BUTTON;
  }
}
