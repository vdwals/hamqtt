package de.vdw.it.hamqtt.devices.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.vdw.it.hamqtt.devices.EntityClass;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public abstract class AbstractSensorEntity extends AbstractAvailabilityEntity {

  /** Flag which defines if the entity should be enabled when first added. */
  Boolean enabledByDefault;

  /**
   * Defines the number of seconds after the sensor’s state expires, if it’s not updated. After
   * expiry, the sensor’s state becomes unavailable.
   */
  @JsonProperty("exp_aft")
  Long expireAfter;

  /**
   * Sends update events (which results in update of state object’s last_changed) even if the
   * sensor’s state hasn’t changed. Useful if you want to have meaningful value graphs in history or
   * want to create an automation that triggers on every incoming state message (not only when the
   * sensor’s new state is different to the current one).
   */
  @JsonProperty("frc_upd")
  Boolean forceUpdate;

  @JsonIgnore
  public EntityClass getClassName() {
    return EntityClass.SENSOR;
  }
}
