package de.vdw.it.hamqtt.devices.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.vdw.it.hamqtt.devices.IDeviceClass;
import de.vdw.it.hamqtt.devices.Units;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.Collections;
import java.util.List;

import static de.vdw.it.hamqtt.devices.Units.*;

@Value
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Sensor extends AbstractSensorEntity {

  @AllArgsConstructor
  public enum DeviceClass implements IDeviceClass {
    /** Percentage of battery that is left. */
    battery(PERCENT),
    current(AMPERE),
    energy(List.of(WATT_PER_HOUR, KILO_WATT_PER_HOUR, MEGA_WATT_PER_HOUR)),
    humidity(PERCENT),
    illuminance(List.of(LUMEN, LUX)),
    signal_strength(List.of(DECIBEL, DECIBEL_METER)),
    temperature(List.of(CELSIUS, FAHRENHEIT)),
    power(List.of(WATT, KILO_WATT)),
    power_factor(PERCENT),
    pressure(List.of(BAR, MILLI_BAR, CENTI_BAR, HECTO_PASCAL, PASCAL, KILO_PASCAL, INHG, PSI)),
    timestamp,
    date,
    voltage(VOLT),
    carbon_monoxide(PARS_PER_MILLION),
    carbon_dioxide(PARS_PER_MILLION),
    aqi,
    frequency(List.of(HZ, KILO_HZ, MEGA_HZ, GIGA_HZ)),
    gas(List.of(CUBIC_FEET, CUBIC_METER)),
    monetary,
    nitrogen_dioxide(MICRO_GRAM_PER_CUBIC_METER),
    nitrogen_monoxide(MICRO_GRAM_PER_CUBIC_METER),
    nitrous_oxide(MICRO_GRAM_PER_CUBIC_METER),
    ozone(MICRO_GRAM_PER_CUBIC_METER),
    pm1(MICRO_GRAM_PER_CUBIC_METER),
    pm25(MICRO_GRAM_PER_CUBIC_METER),
    pm10(MICRO_GRAM_PER_CUBIC_METER),
    sulphur_dioxide(MICRO_GRAM_PER_CUBIC_METER),
    volatile_organic_compounds(MICRO_GRAM_PER_CUBIC_METER);

    @Getter
    private final List<Units> compatibleUnits;

    DeviceClass() {
      compatibleUnits = Collections.emptyList();
    }

    DeviceClass(Units unit) {
      compatibleUnits = List.of(unit);
    }
  }

  public enum StateClass {
    measurement,
    total,
    total_increasing
  }

  @JsonProperty("dev_cla")
  DeviceClass deviceClass;

  String lastResetValueTemplate;

  @JsonProperty("stat_cla")
  StateClass stateClass;

  /**
   * An ID that uniquely identifies this Number. If two Numbers have the same unique ID Home
   * Assistant will raise an exception.
   */
  @JsonProperty("unit_of_meas")
  String unitOfMeasurement;
}
