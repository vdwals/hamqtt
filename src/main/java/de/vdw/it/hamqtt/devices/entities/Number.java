package de.vdw.it.hamqtt.devices.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.vdw.it.hamqtt.devices.EntityClass;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Number extends AbstractCommandEntity {
  /** Minimum value. Default 1 */
  float min;

  /** Maximum value. Default 100 */
  float max;

  /** A special payload that resets the state to None when received on the state_topic. */
  Object payload_reset;

  /** Step value. Smallest value 0.001. Default: 1 */
  float step;

  /**
   * An ID that uniquely identifies this Number. If two Numbers have the same unique ID Home
   * Assistant will raise an exception.
   */
  @JsonProperty("unit_of_meas")
  String unitOfMeasurement;

  @Override
  public EntityClass getClassName() {
    return EntityClass.NUMBER;
  }
}
