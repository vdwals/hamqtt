package de.vdw.it.hamqtt.devices.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.vdw.it.hamqtt.devices.EntityClass;
import de.vdw.it.hamqtt.devices.IDeviceClass;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Switch extends AbstractCommandEntity {

  public enum DeviceClass implements IDeviceClass {
    OUTLET, SWITCH
  }

  DeviceClass deviceClass;

  /**
   * The payload that represents on state. If specified, will be used for both comparing to the
   * value in the state_topic (see value_template and state_on for details) and sending as on
   * command to the command_topic.
   */
  @JsonProperty("pl_on")
  String payloadOn;

  /**
   * The payload that represents off state. If specified, will be used for both comparing to the
   * value in the state_topic (see value_template and state_off for details) and sending as off
   * command to the command_topic.
   */
  @JsonProperty("pl_off")
  String payloadOff;

  /**
   * The payload that represents the on state. Used when value that represents on state in the
   * state_topic is different from value that should be sent to the command_topic to turn the device
   * on.
   *
   * <p>
   * Default: payload_on if defined, else ON
   */
  @JsonProperty("stat_on")
  String stateOn;

  /**
   * The payload that represents the off state. Used when value that represents off state in the
   * state_topic is different from value that should be sent to the command_topic to turn the device
   * off.
   *
   * <p>
   * Default: payload_off if defined, else OFF
   */
  @JsonProperty("stat_off")
  String stateOff;

  @Override
  public EntityClass getClassName() {
    return EntityClass.SWITCH;
  }

  /**
   * Switch currently not supports command template, therefore return value is fixed to null.
   *
   * @return null
   */
  public String getCommandTemplate() {
    return null;
  }
}
