package de.vdw.it.hamqtt.devices.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.vdw.it.hamqtt.devices.EntityClass;
import de.vdw.it.hamqtt.devices.IDeviceClass;
import de.vdw.it.hamqtt.devices.Payload;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Value
@SuperBuilder
@Jacksonized
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BinarySensor extends AbstractSensorEntity {

  public enum DeviceClass implements IDeviceClass {
    /** on means low, off means normal */
    battery,
    /** on means charging, off means not charging */
    battery_charging,
    /** on means cold, off means normal */
    cold,
    /** on means connected, off means disconnected */
    connectivity,
    /** on means open, off means closed */
    door,
    garage_door,
    gas,
    heat,
    light,
    lock,
    moisture,
    motion,
    moving,
    occupancy,
    opening,
    plug,
    presence,
    problem,
    safety,
    smoke,
    sound,
    vibration,
    window,
    power
  }

  /**
   * Sets the class of the device, changing the device state and icon that is displayed on the
   * frontend.
   */
  @JsonProperty("dev_cla")
  DeviceClass deviceClass;

  /**
   * For sensors that only send on state updates (like PIRs), this variable sets a delay in seconds
   * after which the sensor’s state will be updated back to off.
   */
  @JsonProperty("off_dly")
  Integer offDelay;

  /**
   * The string that represents the off state. It will be compared to the message in the state_topic
   * (see value_template for details)
   */
  @JsonProperty("pl_off")
  String payloadOff;

  /**
   * The string that represents the on state. It will be compared to the message in the state_topic
   * (see value_template for details)
   */
  @JsonProperty("pl_on")
  String payloadOn;

  @Override
  public boolean setValue(Object value) {
    // Set string of value if type is payload
    if (value instanceof Payload) {
      return super.setValue(((Payload) value).toString());
    } else {
      return super.setValue(value);
    }
  }

  @Override
  public EntityClass getClassName() {
    return EntityClass.BINARY_SENSOR;
  }
}
