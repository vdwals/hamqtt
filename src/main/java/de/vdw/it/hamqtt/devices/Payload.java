package de.vdw.it.hamqtt.devices;


public enum Payload {
  ON, OFF, INSTALL;

  @Override
  public String toString() {
    return name();
  }
}
