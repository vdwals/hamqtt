package de.vdw.it.hamqtt.devices.entities;

import de.vdw.it.hamqtt.devices.EntityClass;
import lombok.EqualsAndHashCode;
import lombok.Singular;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Value
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Select extends AbstractCommandEntity {
  /** List of options that can be selected. */
  @Singular List<String> options;

  @Override
  public EntityClass getClassName() {
    return EntityClass.SELECT;
  }
}
