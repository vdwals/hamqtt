package de.vdw.it.hamqtt.devices.entities;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonIgnore;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.EntityClass;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@EqualsAndHashCode
@ToString
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public abstract class AbstractEntity {
  /**
   * The maximum QoS level of the state topic. Default is 0 and will also be used to publishing
   * messages.
   */
  Integer qos;

  /**
   * Information about the device this Number is a part of to tie it into the device registry. Only
   * works through MQTT discovery and when unique_id is set. At least one of identifiers or
   * connections must be present to identify the device.
   */
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @NonFinal
  Device device;

  String encoding;

  @NonNull
  String objectId;

  @JsonIgnore
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @NonFinal
  String nodeId;

  @JsonIgnore
  @EqualsAndHashCode.Exclude
  @NonFinal
  Object value;

  @JsonIgnore
  public abstract EntityClass getClassName();

  public boolean setValue(Object value) {
    boolean valueChanged = !Objects.equals(this.value, value);

    this.value = value;

    return valueChanged;
  }
}
