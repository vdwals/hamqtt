package de.vdw.it.hamqtt.devices.entities;

import de.vdw.it.hamqtt.devices.EntityClass;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * Adds a pure value to the data JSON. ClassName of entity is required for the value to appear in
 * the correct data JSON. This is for fixed values without corresponding configuration like
 * last_reset.
 */
@Value
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RawEntity extends AbstractAvailabilityEntity {
  @NonNull EntityClass className;
}
