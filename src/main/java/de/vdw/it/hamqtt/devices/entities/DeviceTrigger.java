package de.vdw.it.hamqtt.devices.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.vdw.it.hamqtt.devices.EntityClass;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DeviceTrigger extends AbstractEntity {

  @Override
  public EntityClass getClassName() {
    return EntityClass.DEVICE_TRIGGER;
  }

  @RequiredArgsConstructor
  /** Type entries supported by Home Assistant Frontend. */
  public enum Type {
    BUTTON_SHORT_PRESS("button_short_press"),
    BUTTON_SHORT_RELEASE("button_short_release"),
    BUTTON_LONG_PRESS("button_long_press"),
    BUTTON_LONG_RELEASE("button_long_release"),
    BUTTON_DOUBLE_PRESS("button_double_press"),
    BUTTON_TRIPLE_PRESS("button_triple_press"),
    BUTTON_QUADRUPLE_PRESS("button_quadruple_press"),
    BUTTON_QUINTUPLE_PRESS("button_quintuple_press");

    @Getter private final String value;
  }

  @RequiredArgsConstructor
  /** Subtype entries supported by Home Assistant Frontend. */
  public enum SubType {
    TURN_ON("turn_on"),
    TURN_OFF("turn_off"),
    BUTTON_1("button_1"),
    BUTTON_2("button_2"),
    BUTTON_3("button_3"),
    BUTTON_4("button_4"),
    BUTTON_5("button_5"),
    BUTTON_6("button_6");

    @Getter private final String value;
  }

  @JsonProperty("atype")
  String automationType = "trigger";

  /** Optional payload to match the payload being sent over the topic. */
  @JsonProperty("pl")
  String payload;

  /** The MQTT topic subscribed to receive trigger events. */
  @JsonProperty("t")
  @NonNull
  String topic;

  /**
   * The type of the trigger, e.g. button_short_press. Entries supported by the frontend:
   * button_short_press, button_short_release, button_long_press, button_long_release,
   * button_double_press, button_triple_press, button_quadruple_press, button_quintuple_press. If
   * set to an unsupported value, will render as subtype type, e.g. button_1 spammed with type set
   * to spammed and subtype set to button_1
   */
  @NonNull String type;

  /**
   * The subtype of the trigger, e.g. button_1. Entries supported by the frontend: turn_on,
   * turn_off, button_1, button_2, button_3, button_4, button_5, button_6. If set to an unsupported
   * value, will render as subtype type, e.g. left_button pressed with type set to
   * button_short_press and subtype set to left_button
   */
  @JsonProperty("stype")
  @NonNull String subtype;
}
