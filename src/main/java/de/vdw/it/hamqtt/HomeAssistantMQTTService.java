package de.vdw.it.hamqtt;

import de.vdw.it.hamqtt.devices.Device;

public interface HomeAssistantMQTTService {
  void addCommandListener(ICommandListener commandListener);

  void addDevice(Device device);

  void addListener(IMqttUpdateListener mqttUpdateListener);

  void connect();

  void disconnect();

  void publishConfigs();

  void publishValues();
}
