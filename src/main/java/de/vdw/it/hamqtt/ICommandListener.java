package de.vdw.it.hamqtt;

import de.vdw.it.hamqtt.devices.Device;

import java.util.List;

public interface ICommandListener {

  /**
   * Is called if the topics of related devices receives a message.
   *
   * @param topic Topic of message
   * @param message Message of topic
   */
  void received(String topic, byte[] message);

  List<Device> getDevices();
}
