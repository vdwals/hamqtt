package de.vdw.it.hamqtt.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.javalite.common.Util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class TopicUtils {
  static final String RELATIVE_TOPIC = "~";

  public static final String COMMAND_TOPIC = "set";

  public static final String DELIMITER = "/";

  public static String buildTopic(String... topics) {
    return buildTopic(Arrays.asList(topics));
  }

  public static String buildTopic(List<Object> topics) {
    List<Object> filteredList =
        topics.stream().filter(ObjectUtils::anyNotNull).collect(Collectors.toList());
    return Util.join(filteredList, DELIMITER);
  }

  public static String getRelativeTopic(String topic) {
    return buildTopic(RELATIVE_TOPIC, topic);
  }

  public static String removeRelativeTopic(String topic) {
    return StringUtils.remove(topic, RELATIVE_TOPIC + DELIMITER);
  }
}
