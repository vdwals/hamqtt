package de.vdw.it.hamqtt.utils;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.paho.client.mqttv3.MqttException;
import de.vdw.it.hamqtt.HomeAssistantMQTTService;
import de.vdw.it.hamqtt.Settings;
import de.vdw.it.hamqtt.services.CommandService;
import de.vdw.it.hamqtt.services.ConfigGenerator;
import de.vdw.it.hamqtt.services.HomeAssistantService;
import de.vdw.it.hamqtt.services.MqttService;
import de.vdw.it.hamqtt.services.TopicGenerator;
import de.vdw.it.hamqtt.services.ValueGenerator;
import eu.lestard.easydi.EasyDI;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ServiceFactory {

  /**
   * Creates a new MQTT-Service connection
   *
   * @param mqttHost Host name or IP of MQTT Broker Host
   * @param mqttPort Port of MQTT Broker Host
   * @param mqttUsername Username for MQTT Broker authentication
   * @param mqttPassword Password for MQTT Broker authentication
   * @param baseTopic Default topic where the EntityGroup values will be published
   * @param discoveryTopic Topic where the EntityGroup configuration is published and is looked up
   *        by home assistant
   * @param sensorName Name of your Sensor connecting with this service to check the availability
   *        status.
   * @return MQTT-Service ready to connect.
   */
  public HomeAssistantMQTTService createHomeAssistantMQTTService(String mqttHost, String mqttPort,
      String mqttUsername, char[] mqttPassword, String baseTopic, String discoveryTopic,
      String sensorName, String mqttProtocoll) throws MqttException {
    Settings.SettingsBuilder settingsBuilder = Settings.builder().mqttHost(mqttHost);
    if (StringUtils.isNotBlank(baseTopic)) {
      settingsBuilder.baseTopic(baseTopic);
    }
    if (StringUtils.isNotBlank(discoveryTopic)) {
      settingsBuilder.discoveryTopic(discoveryTopic);
    }
    if (StringUtils.isNotBlank(sensorName)) {
      settingsBuilder.sensorName(sensorName);
    }
    if (StringUtils.isNotBlank(mqttPort)) {
      settingsBuilder.mqttPort(mqttPort);
    }
    if (StringUtils.isNotBlank(mqttUsername)) {
      settingsBuilder.mqttUsername(mqttUsername);
    }
    if (mqttPassword != null) {
      settingsBuilder.mqttPassword(mqttPassword);
    }
    if (StringUtils.isNotBlank(mqttProtocoll)) {
      settingsBuilder.mqttProtocoll(mqttProtocoll);
    }

    EasyDI easyDI = new EasyDI();

    easyDI.bindInstance(Settings.class, settingsBuilder.build());
    easyDI.markAsSingleton(CommandService.class);
    easyDI.markAsSingleton(ConfigGenerator.class);
    easyDI.markAsSingleton(MqttService.class);
    easyDI.markAsSingleton(TopicGenerator.class);
    easyDI.markAsSingleton(ValueGenerator.class);

    return easyDI.getInstance(HomeAssistantService.class);
  }
}
