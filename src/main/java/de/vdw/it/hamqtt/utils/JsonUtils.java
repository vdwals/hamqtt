package de.vdw.it.hamqtt.utils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.experimental.UtilityClass;

@UtilityClass
public class JsonUtils {

  public static final ObjectMapper jsonMapper =
      JsonMapper.builder()
          .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
          .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
          .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
          .serializationInclusion(Include.NON_NULL)
          .propertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE)
          .addModules(new Jdk8Module(), new JavaTimeModule())
          .build();

  @SuppressWarnings("unused")
  public static String toSnakeCase(String s) {
    return s.replaceAll("([a-z])([A-Z]+)", "$1_$2").toLowerCase();
  }

  public static String toJsonTemplate(String s) {
    if (s == null) {
      return null;
    }
    return String.format("{{ value_json.%s }}", s);
  }

  public static String toJsonAttributeTemplate(String s) {
    if (s == null) {
      return null;
    }
    return String.format("{{ value_json.%s | tojson }}", s);
  }
}
