package de.vdw.it.hamqtt;

import org.apache.commons.lang3.ObjectUtils;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Builder
@Value
public class Settings {

  @Builder.Default
  @NonNull
  String discoveryTopic = "homeassistant", mqttProtocoll = "mqtt";

  @NonNull
  String sensorName;

  String baseTopic;

  @NonNull
  String mqttHost;

  @Builder.Default
  @NonNull
  String mqttPort = "1883";

  String mqttUsername;

  @Builder.Default
  char[] mqttPassword = new char[0];

  public String getBaseTopic() {
    return ObjectUtils.firstNonNull(this.baseTopic, this.sensorName, this.discoveryTopic);
  }
}
