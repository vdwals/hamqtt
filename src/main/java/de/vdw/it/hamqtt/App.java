package de.vdw.it.hamqtt;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import org.eclipse.paho.client.mqttv3.MqttException;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.entities.AbstractAvailabilityEntity;
import de.vdw.it.hamqtt.devices.entities.BinarySensor;
import de.vdw.it.hamqtt.devices.entities.Switch;
import de.vdw.it.hamqtt.utils.JsonUtils;
import de.vdw.it.hamqtt.utils.ServiceFactory;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

public class App implements Callable<Integer> {

  private static class Sensor {
    @Option(
        names = {"-di", "--deviceId"},
        description = "EntityGroup id of sensor",
        required = true)
    String deviceId;

    @Option(
        names = {"-id", "--objectId"},
        description = "Object id of sensor",
        required = true)
    String objectId;

    @Option(
        names = {"-t", "--type"},
        description = "Type of sensor: Binary_Sensor, Sensor",
        required = true)
    String type;

    @Parameters(
        description = "Further attributes. E.g.: \"unit_of_measurement=C,icon=hass:temp\"",
        split = ",",
        arity = "0..*")
    Map<String, String> sensorAttributes;

    private Map<String, String> getAttributes() {
      HashMap<String, String> resultMap = new HashMap<>(this.sensorAttributes);
      resultMap.put("object_id", this.objectId);
      return resultMap;
    }
  }

  @Option(
      names = {"-d", "--discoveryTopic"},
      description = "sets the discovery topic of homeassistant. Default: 'homeassistant'")
  String discoveryTopic;

  @Option(
      names = {"-n", "--name"},
      description = "Name of the sensor node")
  String sensorName;

  @Option(
      names = {"-b", "--baseTopic"},
      description =
      "Sets the base topic to publish sensor values. Default is sensor node name or discovery topic")
  String baseTopic;

  @Option(
      names = {"-h", "--host"},
      description = "MQTT Host",
      required = true)
  String mqttHost;

  @Option(
      names = {"-p", "--port"},
      description = "MQTT Port. Default: 1883")
  String mqttPort;

  @Option(
      names = {"-pr", "--protocoll"}, description = "MQTT protocoll. Default: mqtt")
  String mqttProtocoll;

  @Option(
      names = {"-u", "--username"},
      description = "Username for MQTT-Server")
  String mqttUsername;

  @Option(
      names = {"-pw", "--password"},
      description = "Password for MQTT-Server")
  String mqttPassword;

  @Option(
      names = {"-o", "--oneshot"},
      description = "Set if connection should be closed after sensor value publishing.")
  boolean oneShot;

  @ArgGroup(multiplicity = "1..*", exclusive = false)
  List<Sensor> entities;

  @Option(
      names = {"-v", "--values"},
      description =
      "Map of sensor values as key=value pairs, separated by '|' with object_id as key. E.g.: \"online=true|temperature=28.7\"",
      required = true)
  Map<String, String> values;

  @Override
  public Integer call() throws IOException, MqttException {
    HomeAssistantMQTTService s =
        ServiceFactory.createHomeAssistantMQTTService(
            this.mqttHost,
            this.mqttPort,
            this.mqttUsername,
            this.mqttPassword == null ? null : this.mqttPassword.toCharArray(),
                this.baseTopic,
                this.discoveryTopic,
                this.sensorName, this.mqttProtocoll);

    s.connect();

    Map<String, Device> idDeviceMap = new HashMap<>();
    Map<String, Device> objectIdDeviceMap = new HashMap<>();

    this.entities.forEach(
        entity -> {
          AbstractAvailabilityEntity entityDto = null;
          switch (entity.type) {
            case "Binary_Sensor" -> entityDto = JsonUtils.jsonMapper.convertValue(entity.getAttributes(),
                BinarySensor.class);
            case "Sensor" -> entityDto = JsonUtils.jsonMapper.convertValue(entity.getAttributes(),
                de.vdw.it.hamqtt.devices.entities.Sensor.class);
            case "Switch" -> entityDto = JsonUtils.jsonMapper.convertValue(entity.getAttributes(),
                Switch.class);
            default -> {
            }
          }

          if (entityDto != null) {
            if (!idDeviceMap.containsKey(entity.deviceId)) {
              Device device = Device.builder().nodeId(entity.deviceId).build();
              idDeviceMap.put(entity.deviceId, device);
            }
            objectIdDeviceMap.put(entity.objectId, idDeviceMap.get(entity.deviceId));
            idDeviceMap.get(entity.deviceId).addEntity(entityDto);
          }
        });

    idDeviceMap.values().forEach(s::addDevice);

    s.publishConfigs();

    this.values.forEach(
        (objectId, value) -> {
          Device entityGroup = objectIdDeviceMap.get(objectId);
          entityGroup.getConfigByObjectId(objectId).ifPresent(entity -> entity.setValue(value));
        });

    s.publishValues();
    s.disconnect();

    return 1;
  }
}
