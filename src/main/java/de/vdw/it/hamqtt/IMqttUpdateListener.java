package de.vdw.it.hamqtt;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface IMqttUpdateListener {

  void connected();

  void connectionLost();

  void disconnected();

  void received(String topic, MqttMessage message);
}
