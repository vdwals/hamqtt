package de.vdw.it.hamqtt.services;

import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Singleton;
import org.apache.commons.collections4.MapUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import de.vdw.it.hamqtt.EnumValue;
import de.vdw.it.hamqtt.availability.AvailabilityEnum;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.utils.JsonUtils;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Value
@Singleton
public class ValueGenerator {
  TopicGenerator topicGenerator;

  @SuppressWarnings({"unchecked", "rawtypes"})
  private Map<String, Object> createTopicMap(String pathId, Map<String, Object> nodeValueMap) {
    String fullStateTopic = this.topicGenerator.getFullAvailabilityTopic(pathId);

    Map<String, Object> valueMap = new HashMap<>();

    if (MapUtils.isEmpty(nodeValueMap)) {
      valueMap.put(fullStateTopic, AvailabilityEnum.OFFLINE);

    } else {
      valueMap.put(fullStateTopic, AvailabilityEnum.ONLINE);

      String fullDataTopic = this.topicGenerator.getFullDataTopic(pathId);
      valueMap.merge(fullDataTopic, nodeValueMap, (oldValue, newMap) -> {
        if (oldValue instanceof Map oldMap) {
          oldMap.putAll((Map<String, Object>) newMap);
          return oldMap;
        }
        return newMap;
      });
    }

    return valueMap;
  }

  private SimpleEntry<String, byte[]> createValueEntry(Entry<String, Object> entry) {
    try {
      String value;
      if (entry.getValue() instanceof EnumValue) {
        value = ((AvailabilityEnum) entry.getValue()).toString();
      } else {
        value = JsonUtils.jsonMapper.writeValueAsString(entry.getValue());
      }

      return new AbstractMap.SimpleEntry<>(entry.getKey(), value.getBytes(StandardCharsets.UTF_8));
    } catch (JsonProcessingException e) {
      log.error(e.getMessage(), e);
    }
    return null;
  }

  public Map<String, byte[]> getValueMessages(Collection<Device> devices) {

    return devices.stream()
        .flatMap(device -> device.getEntities().entrySet().stream().map(entry -> {
          String nodeId = entry.getKey();

          Map<String, Object> nodeValueMap = device.getEntityValues(nodeId);

          return createTopicMap(nodeId, nodeValueMap);
        }).map(Map::entrySet).flatMap(Set::stream).map(this::createValueEntry)
            .filter(Objects::nonNull))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
