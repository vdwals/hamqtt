package de.vdw.it.hamqtt.services;

import de.vdw.it.hamqtt.HomeAssistantMQTTService;
import de.vdw.it.hamqtt.ICommandListener;
import de.vdw.it.hamqtt.IMqttUpdateListener;
import de.vdw.it.hamqtt.Settings;
import de.vdw.it.hamqtt.availability.Availability;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.Payload;
import de.vdw.it.hamqtt.devices.entities.BinarySensor;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class HomeAssistantService implements HomeAssistantMQTTService {
  Settings settings;
  MqttService mqttService;
  CommandService commandService;
  TopicGenerator topicGenerator;
  ValueGenerator valueGenerator;
  ConfigGenerator configGenerator;

  Set<Device> devices = new HashSet<>();

  public HomeAssistantService(
      Settings settings,
      MqttService mqttService,
      CommandService commandService,
      TopicGenerator topicGenerator,
      ValueGenerator valueGenerator,
      ConfigGenerator configGenerator
  ) {
    this.configGenerator = configGenerator;
    this.valueGenerator = valueGenerator;
    this.settings = settings;
    this.mqttService = mqttService;
    this.commandService = commandService;
    this.topicGenerator = topicGenerator;

    String nodeId = "ha-mqtt-server";
    String availabilityTopic = this.topicGenerator.getFullAvailabilityTopic(nodeId);

    Availability serverAvailability = Availability.builder()
        .topic(availabilityTopic)
        .build();

    Device serverDevice = Device.builder()
        .name("HA-MQTT-Server")
        .swVersion("3.2.0")
        .nodeId(nodeId)
        .defaultAvailability(serverAvailability)
        .build();

    BinarySensor onlineSensor = BinarySensor.builder()
        .name(this.settings.getSensorName())
        .deviceClass(BinarySensor.DeviceClass.connectivity)
        .objectId("ha-mqtt-server_" + this.settings.getSensorName())
        .build();

    serverDevice.addEntity(onlineSensor);

    this.devices.add(serverDevice);
    onlineSensor.setValue(Payload.ON);

    this.mqttService.setServerAvailability(serverAvailability);
    log.info(String.format("Sensor generated: %s", serverDevice));
  }

  @Override
  public void addCommandListener(ICommandListener commandListener) {
    this.commandService.addCommandListener(commandListener);
  }

  @Override
  public void addDevice(Device device) {
    if (this.devices.add(device)) {
      this.commandService.getCommandTopicsFromDevice(device)
          .forEach(commandTopic -> {
            log.info("Subscribing to topic {} for device {}", commandTopic, device);
            this.mqttService.subscribe(commandTopic);
          });
    }
  }

  @Override
  public void addListener(IMqttUpdateListener mqttUpdateListener) {
    this.mqttService.addListener(mqttUpdateListener);
  }

  @Override
  public void connect() {
    this.mqttService.connect();
    this.publishConfigs();
  }

  @Override
  public void disconnect() {
    this.mqttService.disconnect();
  }

  @Override
  public void publishConfigs() {
    log.debug("Publishing configs of {} devices.", this.devices.size());

    Map<String, byte[]> configMessages = this.configGenerator.getConfigMessages(this.devices);
    this.mqttService.publish(configMessages, true);
  }

  @Override
  public void publishValues() {
    log.debug("Publishing values of {} devices.", this.devices.size());

    Map<String, byte[]> messages = this.valueGenerator.getValueMessages(this.devices);
    this.mqttService.publish(messages, false);
  }
}
