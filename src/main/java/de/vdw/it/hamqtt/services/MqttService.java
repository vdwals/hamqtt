package de.vdw.it.hamqtt.services;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.inject.Singleton;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import de.vdw.it.hamqtt.IMqttUpdateListener;
import de.vdw.it.hamqtt.Settings;
import de.vdw.it.hamqtt.availability.Availability;
import de.vdw.it.hamqtt.availability.AvailabilityEnum;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Value
@Singleton
public class MqttService implements IMqttMessageListener, MqttCallback {
  Settings settings;

  CommandService commandService;

  Set<IMqttUpdateListener> listener = new HashSet<>();

  MqttConnectOptions options = new MqttConnectOptions();

  MqttClient mqtt;

  Queue<String> subscribeTopics = new LinkedList<>();


  ExecutorService executorService = Executors.newCachedThreadPool();

  @NonFinal
  @Setter
  Availability serverAvailability;

  public MqttService(Settings settings, CommandService commandService) throws MqttException {
    this.settings = settings;
    this.commandService = commandService;

    // Build mqtt service
    this.mqtt = new MqttClient(String.format("%s://%s:%s", this.settings.getMqttProtocoll(),
        this.settings.getMqttHost(), this.settings.getMqttPort()), this.settings.getSensorName());
    this.mqtt.setCallback(this);
  }

  public void addListener(IMqttUpdateListener mqttUpdateListener) {
    this.listener.add(mqttUpdateListener);
  }

  public void connect() {
    if (StringUtils.isNotBlank(this.settings.getMqttUsername())
        && this.settings.getMqttPassword().length > 0) {
      this.options.setUserName(this.settings.getMqttUsername());
      this.options.setPassword(this.settings.getMqttPassword());
    }
    if (this.serverAvailability != null) {
      this.options.setWill(this.serverAvailability.getTopic(), AvailabilityEnum.OFFLINE.toBytes(),
          0, true);
    }

    reconnect();

    this.listener.forEach(IMqttUpdateListener::connected);
  }

  @Override
  public void connectionLost(Throwable cause) {
    log.info("Connection lost", cause);
    this.listener.forEach(IMqttUpdateListener::connectionLost);
  }

  @Override
  public void deliveryComplete(IMqttDeliveryToken token) {
    // TODO Auto-generated method stub

  }

  public void disconnect() {
    try {
      this.mqtt.disconnect();
      this.listener.forEach(IMqttUpdateListener::disconnected);
      log.info("Disconnected");
    } catch (MqttException e) {
      log.error(e.getMessage(), e);
    }
  }

  @Override
  public void messageArrived(String topic, MqttMessage message) {
    byte[] payload = message.getPayload();
    log.debug("MQTT Message received: {} = {}", topic, new String(payload));
    this.listener.forEach(listener -> listener.received(topic, message));

    this.commandService.commandReceived(topic, message.getPayload());
  }

  public void publish(Map<String, byte[]> messages, boolean retain) {
    if (!this.mqtt.isConnected()) {
      reconnect();
    }

    messages.entrySet().parallelStream()
        .map(entry -> new Message(entry.getKey(), entry.getValue(), retain)).forEach(message -> {
          if (message.getPayload().length > 255) {
            log.warn("Message exceeds limit of 255 characters {}", message.asString());
          }
          executorService.submit(() -> {
            try {
              this.mqtt.publish(message.getTopic(), message.getPayload(), 0, message.isRetain());

            } catch (MqttException e) {
              log.error(e.getMessage(), e);
            }
          });
        });
  }

  private void reconnect() {
    try {
      log.debug("Connecting");
      this.mqtt.connect(this.options);

      /*
       * Subscribe to all topics requested to subscribe when offline
       */
      String commandTopic;
      while ((commandTopic = this.subscribeTopics.poll()) != null) {
        subscribe(commandTopic);
      }

    } catch (MqttException e) {
      log.error("Reconnection failed:", e);
    }
  }

  public void subscribe(String commandTopic) {
    if (this.mqtt.isConnected()) {
      try {
        this.mqtt.subscribe(commandTopic, this);
      } catch (MqttException e) {
        log.error(e.getMessage(), e);
      }
    } else {
      this.subscribeTopics.add(commandTopic);
    }
  }
}
