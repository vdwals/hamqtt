package de.vdw.it.hamqtt.services;

import java.nio.charset.StandardCharsets;
import lombok.Value;

@Value
public class Message {
  String topic;
  byte[] payload;
  boolean retain;

  public String asString() {
    return new String(this.payload, StandardCharsets.UTF_8);
  }
}
