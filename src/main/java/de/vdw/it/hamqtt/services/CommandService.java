package de.vdw.it.hamqtt.services;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import javax.inject.Singleton;
import org.apache.commons.collections4.CollectionUtils;
import de.vdw.it.hamqtt.ICommandListener;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.entities.AbstractCommandEntity;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Value
@Singleton
public class CommandService {

  ConfigGenerator configGenerator;

  List<ICommandListener> commandListeners = new LinkedList<>();

  ExecutorService executorService = Executors.newCachedThreadPool();

  public void addCommandListener(ICommandListener commandListener) {
    commandListeners.add(commandListener);
  }

  public void commandReceived(String topic, byte[] payload) {
    Map<String, List<ICommandListener>> topicMap = getTopicMap();

    CollectionUtils.emptyIfNull(topicMap.get(topic)).forEach(listener -> {
      log.debug("Publish message to listener.");
      log.trace("Listener: {}", listener);
      executorService.submit(() -> {
        listener.received(topic, payload);
      });
    });
  }

  public Set<String> getCommandTopicsFromDevice(Device device) {
    if (device == null) {
      return Collections.emptySet();
    }
    return device.getAvailabilityEntityStream()
        .filter(entity -> entity instanceof AbstractCommandEntity)
        .map(entity -> (AbstractCommandEntity) entity).map(configGenerator::getFullCommandTopic)
        .collect(Collectors.toSet());
  }

  public Map<String, List<ICommandListener>> getTopicMap() {
    Map<String, List<ICommandListener>> topicsMap = new HashMap<>();

    // Reduce device to entities
    Map<ICommandListener, List<AbstractCommandEntity>> entityMap = commandListeners.stream()
        .collect(Collectors.toMap(cl -> cl,
            cl -> cl.getDevices().stream().flatMap(Device::getAvailabilityEntityStream)
                .filter(entity -> entity instanceof AbstractCommandEntity)
                .map(entity -> (AbstractCommandEntity) entity).collect(Collectors.toList())));

    // Switch assignment from listener -> Device -> Entity -> topic to topic -> Listener
    entityMap.entrySet().stream().filter(entry -> !entry.getValue().isEmpty()).flatMap(entry ->
    // Filter entities by command topics
    entry.getValue().stream().map(configGenerator::getFullCommandTopic)
        .map(commandTopic -> new AbstractMap.SimpleEntry<>(commandTopic, entry.getKey())))
        .forEach(entry -> {
          if (!topicsMap.containsKey(entry.getKey())) {
            topicsMap.put(entry.getKey(), new LinkedList<>());
          }

          topicsMap.get(entry.getKey()).add(entry.getValue());
        });

    return topicsMap;
  }
}
