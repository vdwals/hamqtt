package de.vdw.it.hamqtt.services;

import static de.vdw.it.hamqtt.utils.TopicUtils.buildTopic;
import de.vdw.it.hamqtt.Settings;
import java.util.List;
import javax.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Singleton
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TopicGenerator {
  public static final String STATUS_TOPIC = "status";

  public static final String DATA_TOPIC = "data";

  static final String NODES_TOPIC = "nodes";

  String baseTopic;

  public TopicGenerator(Settings settings) {
    this.baseTopic = settings.getBaseTopic();
  }

  public String getFullAvailabilityTopic(String nodeId) {
    return buildTopic(List.of(getFullTopic(nodeId), STATUS_TOPIC));
  }

  public String getFullDataTopic(String nodeId) {
    return buildTopic(List.of(getFullTopic(nodeId), DATA_TOPIC));
  }

  public String getFullTopic(String nodeId) {
    return buildTopic(List.of(this.baseTopic, NODES_TOPIC, nodeId));
  }
}
