package de.vdw.it.hamqtt.services;

import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.inject.Singleton;
import com.fasterxml.jackson.core.JsonProcessingException;
import de.vdw.it.hamqtt.Settings;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.entities.AbstractCommandEntity;
import de.vdw.it.hamqtt.devices.entities.AbstractEntity;
import de.vdw.it.hamqtt.devices.entities.RawEntity;
import de.vdw.it.hamqtt.utils.JsonUtils;
import de.vdw.it.hamqtt.utils.TopicUtils;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
public class ConfigGenerator {
  static final String CONFIG_TOPIC = "config";

  TopicGenerator topicGeneratorService;

  String discoveryTopic;

  public ConfigGenerator(Settings settings, TopicGenerator topicGeneratorService) {
    this.topicGeneratorService = topicGeneratorService;
    this.discoveryTopic = settings.getDiscoveryTopic();
  }

  private String getConfigJson(AbstractEntity entity) throws JsonProcessingException {
    @SuppressWarnings("unchecked")
    Map<String, Object> map = JsonUtils.jsonMapper.convertValue(entity, Map.class);
    map.put("~", getFullTopic(entity));

    return JsonUtils.jsonMapper.writeValueAsString(map);
  }

  public Map<String, byte[]> getConfigMessages(Collection<Device> devices) {

    return devices.stream().flatMap(device -> device.getEntities().entrySet().stream())
        .flatMap(entry -> entry.getValue().stream().filter(entity -> !(entity instanceof RawEntity))
            .map(entity -> {
              String nodeId = entry.getKey();
              String topic = getConfigTopic(entity, nodeId);

              try {
                String config = getConfigJson(entity);

                log.debug("Config generated: {} = {}", topic, config);
                return new AbstractMap.SimpleEntry<>(topic,
                    config.getBytes(StandardCharsets.UTF_8));
              } catch (JsonProcessingException e) {
                log.error(e.getMessage(), e);
              }

              return null;
            }).filter(Objects::nonNull))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  private String getConfigTopic(AbstractEntity config, String nodeId) {
    return TopicUtils.buildTopic(this.discoveryTopic, config.getClassName().toString(), nodeId,
        config.getObjectId(), CONFIG_TOPIC);
  }

  public String getFullCommandTopic(AbstractCommandEntity commandEntity) {
    String fullTopic = getFullTopic(commandEntity);

    String commandTopic = TopicUtils.removeRelativeTopic(commandEntity.getCommandTopic());

    return TopicUtils.buildTopic(fullTopic, commandTopic);
  }

  public String getFullTopic(AbstractEntity entity) {
    return this.topicGeneratorService.getFullTopic(entity.getNodeId());
  }
}
